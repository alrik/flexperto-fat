<?php

use yii\db\Migration;
use yii\db\Schema;

class m160817_200040_add_mobile_number extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'mobile_number', Schema::TYPE_STRING . '(17)');
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'mobile_number');

        return false;
    }
}
