<?php

use yii\db\Migration;
use yii\db\Schema;
use common\services\UserProfileService;
use common\models\User;

class m160821_184052_gravatar_thumbnail extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'gravatar_thumbnail', Schema::TYPE_STRING . '(128)');

        $userProfileService = new UserProfileService();
        $users = User::find()->all();


        foreach ($users as $user) {
            $gravatarThumbnailUrl = $userProfileService->lookupGravatarThumbnailUrl($user);

            if ($gravatarThumbnailUrl) {
                $user->gravatar_thumbnail = $gravatarThumbnailUrl;
                $user->save();
            }
        }
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'gravatar_thumbnail');

        return false;
    }
}
