<?php

use yii\db\Migration;
use yii\db\Schema;

class m160821_205113_profile_image extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'profile_image', Schema::TYPE_STRING . '(255)');
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'profile_image');

        return false;
    }
}
