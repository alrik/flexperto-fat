<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        This is the final application test of <a href="https://flexperto.com">flexperto</a>.
    </p>

    <p><a class="btn btn-lg btn-success" href="https://bitbucket.com/alrik/flexperto-fat">Fork me on Bitbucket</a></p>
</div>
