<?php

use Yii;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
$this->title = 'Account';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $userModel,
        'attributes' => [
            [
                'attribute' => 'gravatar_thumbnail',
                'visible' => !$userModel->profile_image && !!$userModel->gravatar_thumbnail,
                'format' => ['image', ['height' => 100, 'class' => 'img-thumbnail img-circle']]
            ],
            [
                'value' => $userModel->profile_image ? str_replace('@webroot', '', $userModel->profile_image): null,
                'attribute' => 'profile_image',
                'visible' => !!$userModel->profile_image,
                'format' => ['image', ['height' => 100, 'width' => 100, 'class' => 'img-thumbnail img-circle']]
            ],
            'id', 'username', 'email:email',
            [
                'attribute' => 'mobile_number',
                'visible' => !!$userModel->mobile_number
            ], [
                'attribute' => 'created_at',
                'label' => 'Member Since',
                'format' => 'datetime'
            ]
        ]
    ]); ?>

    <?= Html::a('Delete Account', ['account/delete'], [
        'class' => 'btn btn-danger'
    ]) ?>

    <?= Html::a('Edit Profile', ['account/edit-profile'], [
        'class' => 'btn btn-primary'
    ]) ?>
</div>
