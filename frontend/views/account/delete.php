<?php

use Yii;
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'Account Deletion';
$this->params['breadcrumbs'][] = ['label' => 'Account', 'url' => ['account/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= Html::a('YES, please delete my account!', ['account/submit-delete'], [
        'class' => 'btn btn-danger'
    ]) ?>

    <?= Html::a('Cancel', ['account/index'], [
        'class' => 'btn btn-secondary'
    ]) ?>
</div>
