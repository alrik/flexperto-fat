<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\EditProfileForm */

$this->title = 'Edit Profile';
$this->params['breadcrumbs'][] = ['label' => 'Account', 'url' => ['account/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-edit-profile">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'edit-profile-form']); ?>
            <?php
                if ($model->getUser()->profile_image) {
                    printf(
                        "<img src='%s' alt='Profile Image' width='100' height='100' class='profile-image img-circle' />",
                        str_replace('@webroot', '', $model->getUser()->profile_image)
                    );
                }
            ?>
            <?= $form->field($model, 'profileImage')->fileInput(['accept' => 'image/*'])?>
            <?= $form->field($model, 'mobileNumber')->textInput() ?>
            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
