<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\models\User;
use frontend\models\EditProfileForm;

/**
 * Account controller
 */
class AccountController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'delete', 'edit-profile', 'submit-delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * Display account index page.
     *
     * @return string
     */
    public function actionIndex()
    {
        $user = User::findOne(Yii::$app->user->getId());

        return $this->render('index', [
            'userModel' => $user
        ]);
    }

    /**
     * Display the account deletion page.
     *
     * @return string
     */
    public function actionDelete()
    {
        return $this->render('delete');
    }

    /**
     * Perform the account deletion logic; delete the user from database and destroy the current session.
     * After that redirect the user to homepage.
     */
    public function actionSubmitDelete()
    {
        $user = User::findOne(Yii::$app->user->getId());

        $user->delete();

        Yii::$app->user->logout();

        $this->redirect(['site/index']);
    }

    /**
     * Display and perform the profile edit logic.
     *
     * @return string
     */
    public function actionEditProfile()
    {
        try {
            $model = new EditProfileForm(Yii::$app->user->getId());
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->editProfile()) {
                Yii::$app->getSession()->setFlash('success', 'Profile has successfully been updated.');

                // update session users data
                Yii::$app->user->getIdentity()->attributes = $model->getUser()->getAttributes();
            }
        }

        return $this->render('edit-profile', [
            'model' => $model,
        ]);
    }
}
