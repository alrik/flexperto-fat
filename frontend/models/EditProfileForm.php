<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use common\models\User;
use common\services\UserProfileService;

/**
 * Password reset form
 */
class EditProfileForm extends Model
{
    /**
     * @var \common\models\User
     */
    private $user;

    /**
     * @var UserProfileService
     */
    protected $userProfileService;

    /**
     * @var string
     */
    public $mobileNumber;

    /**
     * @var UploadedFile
     */
    public $profileImage;

    /**
     * Creates a profile form model for the given user.
     *
     * @param integer $userId
     * @param array $config name-value pairs that will be used to initialize the object properties
     */
    public function __construct($userId, $config = [])
    {
        $this->user = User::findOne($userId);

        $this->mobileNumber = $this->user->mobile_number;

        parent::__construct($config);
    }

    /**
     * Fetch and return the user profile service.
     *
     * @return UserProfileService
     */
    public function getUserProfileService()
    {
        if ($this->userProfileService === null) {
            $this->userProfileService = Yii::$app->get('userProfileService');
        }

        return $this->userProfileService;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['mobileNumber', 'default', 'value' => null],
            ['mobileNumber', 'filter', 'filter' => function($rawNumber) {
                return preg_replace('/[^0-9+]+/m', '', $rawNumber);
            }, 'skipOnArray' => true],
            ['mobileNumber', 'match', 'pattern' => '/^(\+|\d)[0-9]{7,16}$/'],

            ['profileImage', 'file', 'extensions' => ['png', 'jpg', 'gif']]
        ];
    }

    /**
     * Update the user profile data.
     *
     * @return boolean if password was reset.
     */
    public function editProfile()
    {
        $user = $this->user;

        $user->mobile_number = $this->mobileNumber;

        $uploadedFile = UploadedFile::getInstance($this, 'profileImage');

        if ($uploadedFile) {
            return $this->getUserProfileService()->saveProfileImage($user, $uploadedFile);
        }


        return $user->save();
    }
}
