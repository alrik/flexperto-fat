<?php
namespace frontend\models;

use common\models\User;
use common\services\UserProfileService;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    /**
     * @var string
     */
    public $username;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $password;

    /**
     * @var UserProfileService
     */
    protected $userProfileService = null;

    /**
     * Fetch and return the user profile service.
     *
     * @return UserProfileService
     */
    public function getUserProfileService()
    {
        if ($this->userProfileService === null) {
            $this->userProfileService = Yii::$app->get('userProfileService');;
        }

        return $this->userProfileService;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->gravatar_thumbnail = $this->getUserProfileService()->lookupGravatarThumbnailUrl($user);

            if ($user->save()) {
                return $user;
            }
        }

        return null;
    }
}
