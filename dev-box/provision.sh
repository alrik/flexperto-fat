#!/usr/bin/env bash

# Exit immediately if a command exits with a non-zero status.
set -e

# configuration
APP_DIR=/opt/flexperto-fat/
MYSQL_ROOT_PASSWORD='flexperto$secure'
DATABASE_NAME=yii2advanced
DATABASE_USER=yii
DATABASE_USER_PASSWORD='yii$secure'

# get the latest packages
sudo apt-get -q update
sudo apt-get -qy upgrade
sudo apt-get -qy autoremove

function requirePackage {
    for package in $@; do
        echo -n "Checking required package ${package}."

        if ( ! dpkg-query -l ${package} &> /dev/null ); then
            sudo apt-get -qy install ${package}
        fi
    done
}

function runSQL {
    mysql -uroot -p${MYSQL_ROOT_PASSWORD} -e "$1"
    return $?
}

requirePackage git
requirePackage php5-cli php5-imagick php5-intl php5-mcrypt php5-memcached php5-mysqlnd
requirePackage apache2 libapache2-mod-php5

# allow www-data user to write logs and runtime data
sudo usermod -aG vagrant www-data

sudo php5enmod imagick intl mcrypt memcached mysqlnd


echo "mysql-server mysql-server/root_password password ${MYSQL_ROOT_PASSWORD}" | sudo debconf-set-selections
echo "mysql-server mysql-server/root_password_again password ${MYSQL_ROOT_PASSWORD}" | sudo debconf-set-selections

requirePackage mysql-server

if ! runSQL "SHOW DATABASES LIKE '${DATABASE_NAME}';" | grep -q ${DATABASE_NAME}; then
    echo -n "Create database ${DATABASE_NAME}."
    runSQL "CREATE DATABASE ${DATABASE_NAME}"
    echo -n "Grant database access to ${DATABASE_USER}:${DATABASE_USER_PASSWORD}."
    runSQL "GRANT ALL PRIVILEGES ON ${DATABASE_NAME}.* TO '${DATABASE_USER}'@'localhost' IDENTIFIED BY '${DATABASE_USER_PASSWORD}'"
fi

if ! which composer 2>&1 > /dev/null; then
    curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer
fi

if ( ! composer config -g github-oauth.github.com  &> /dev/null ); then
    if [ -z $GITHUB_OAUTH_TOKEN ]; then
        echo -n "Please provide a public access github oauth token.";
        echo -n "$ GITHUB_OAUTH_TOKEN=YOUR_TOKEN vagrant up";

        exit 1
    fi
fi

if [ ! -z $GITHUB_OAUTH_TOKEN ]; then
    echo -n "Installing new github oauth token."
    composer config -g github-oauth.github.com ${GITHUB_OAUTH_TOKEN}
fi

if ( ! composer global show fxp/composer-asset-plugin 2>&1 > /dev/null ); then
    composer global require fxp/composer-asset-plugin
fi

# trust github.com, to avoid interactive confirmation within composer installment
ssh-keyscan -t rsa github.com > ~/.ssh/known_hosts

composer install -d ${APP_DIR}

if [ ! -f /var/run/flexperto-fat-initialized ]; then
    echo -n "Initialize flexperto FAT application."

    ${APP_DIR}init --env=Development --force
    sudo touch /var/run/flexperto-fat-initialized
fi

# adjust database config
CONFIG_FILE=${APP_DIR}common/config/main-local.php

if ! grep -q ${DATABASE_USER_PASSWORD} ${APP_DIR}common/config/main-local.php; then
    echo "Apply database config."

    sed -i -e "s/'username'.*/'username' => '${DATABASE_USER}',/" \
        -e "s/'password'.*/'password' => '${DATABASE_USER_PASSWORD}',/" \
        $CONFIG_FILE
fi

# execute migrations
${APP_DIR}/yii migrate --interactive=0

if [ ! -f /etc/apache2/sites-enabled/flexperto-fat.conf ] || \
! diff /etc/apache2/sites-enabled/flexperto-fat.conf ${APP_DIR}dev-box/etc/apache2/sites-available/flexperto-fat.conf &> /dev/null; then

    sudo cp ${APP_DIR}dev-box/etc/apache2/sites-available/flexperto-fat.conf /etc/apache2/sites-enabled/flexperto-fat.conf
    sudo a2dissite 000-default
    sudo a2ensite flexperto-fat
    sudo service apache2 reload
fi