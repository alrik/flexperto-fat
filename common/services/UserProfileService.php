<?php
namespace common\services;

use Yii;
use common\models\User;
use yii\base\Exception;
use yii\web\UploadedFile;

class UserProfileService
{
    public $profileImageBasePath = "@webroot/profile-images/";
    public $profileImagePathDepth = 5;

    /**
     * Lookup gravatar profile and return the thumbnail url of a given user.
     *
     * @param User $user
     * @return string|null The gravatar thumbnail url, or null.
     */
    public function lookupGravatarThumbnailUrl(User $user)
    {
        $emailHash = md5(strtolower(trim($user->email)));
        $profileResponse = @file_get_contents("https://www.gravatar.com/${emailHash}.php");

        if ($profileResponse) {
            $profile = unserialize($profileResponse);

            if (isset($profile['entry'][0]['thumbnailUrl'])) {
                return $profile['entry'][0]['thumbnailUrl'];
            }
        }

        return null;
    }

    /**
     * Save profile image of the given user.
     *
     * @param User $user
     * @param UploadedFile $uploadedFile
     * @return bool
     */
    public function saveProfileImage(User $user, UploadedFile $uploadedFile)
    {
        $profileImagePath = $this->profileImageBasePath;
        $oldProfileImagePath = $user->profile_image;

        for ($i = 0, $l = $this->profileImagePathDepth; $i < $l; ++$i) {
            $profileImagePath .= Yii::$app->security->generateRandomString(2) . '/';
        }

        $this->assureDirectoryExists($profileImagePath);
        $profileImagePath .= Yii::$app->security->generateRandomString() . ".{$uploadedFile->extension}";

        $uploadedFile->saveAs(Yii::getAlias($profileImagePath));
        $this->deleteImage($oldProfileImagePath);

        $user->profile_image = $profileImagePath;

        return $user->save();
    }

    /**
     * Delete the profile image of the given user.
     *
     * @param User $user
     * @return bool
     */
    public function deleteProfileImage(User $user)
    {
        if (!$user->profile_image) {
            return true;
        }

        $this->deleteImage($user->profile_image);

        $user->profile_image = null;
        return $user->save();
    }

    /**
     * Delete the given file.
     *
     * @param string $path
     * @return bool
     */
    protected function deleteImage($path)
    {
        $path = Yii::getAlias($path);

        if (is_file($path)) {
            return unlink($path);
        }

        return false;
    }

    /**
     * Recursively create the given directory if it does not yet exist.
     *
     * @param string $directory
     * @throws Exception
     */
    protected function assureDirectoryExists($directory)
    {
        $directory = Yii::getAlias($directory);

        if (!is_dir($directory) && !mkdir($directory, '0755', true)) {
            throw new Exception('Cannot create profile image storage directory.');
        }
    }
}