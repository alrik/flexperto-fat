<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],

        'userProfileService' => [
            'class' => 'common\services\UserProfileService',
            'profileImageBasePath' => '@webroot/profile-images/',
            'profileImagePathDepth' => 5
        ]
    ],
];
